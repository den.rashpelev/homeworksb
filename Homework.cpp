﻿#include <iostream>
#include <ctime>

int main() {
    const int N = 5;
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
        }
    }
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            std::cout << array[i][j];
        }
        std::cout << '\n';
    }
    int calendar = 6; //пример сегодняшней даты
    int row = calendar % N;
    int sum = 0;
    for (int j = 0; j < N; j++)
    {
        sum += array[row][j];
    }
    std::cout << "Summa elementov v stroke s indexom " << row << ": " << sum << '\n';

    return 0;
}